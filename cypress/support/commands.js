Cypress.Commands.add("setTodos", (amount) => {
    const todos = {'todos' : []};
    
    for(let i = 0; i < amount; i++){
        const newTodo = {
            'completed' : false,
            'id'        : Math.ceil(Date.now() * Math.random()),
            'title'     : 'New todo ' + i
        }

        todos.todos.push(newTodo);
    }

    localStorage.setItem('todos-vanillajs', JSON.stringify(todos));
});

