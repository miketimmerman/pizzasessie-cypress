describe('todos', () => {        
    it('should add a new todo', () => {
        cy.visit('/index.html');
        
        cy.get('.new-todo')
            .type('Schrijf een nieuwe test {enter}');
        
        cy.get('.todo-list')
            .find('li')
            .should('have.length', 1);
    });

    it('should have multiple todo', () => {
        cy.setTodos(5);
        
        cy.visit('/index.html');

        cy.get('.todo-list')
            .find('li')
            .should('have.length', 5);
    });

    it('should complete a todo', () => {
        cy.setTodos(5);
        
        cy.visit('/index.html');

        cy.get('.todo-list')
            .find('li')
            .eq(2)
            .find('.toggle')
            .click();

        cy.get('.todo-list')
            .find('li.completed')
            .should('have.length', 1);
    });
});