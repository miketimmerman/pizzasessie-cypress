# Testing the web with Cypress

## Getting started
Create a new project folder. Inside this folder initialize a nodeJS package

	npm init


Install Cypress in your project. 

	npm install cypress --save-dev

** You can also add Cypress globally by replacing `--save-dev` with `-g`. For version managment it is beter to install it inside your project.

Now add a npm script to start the Cypress dashboard


	...
	"scripts": {
	  "cypress:open": "cypress open"
	},
	...

Let's open the Cypress dashboard
	
	npm run cypress:open
	

Because this is your first run, it will create some folders/files in de root of the project:

	cypress.json
	cypress/
		integration/
		screenshots/
		fixtures/
		plugins/
		support/


It also contains of a example test. Let's execute this example. Click `example_spec.js` in the dashboard.

## Cypress and CI

Like other testing tools Cypress can also be added to your continuous integration workflow. It can run headless from the command line. To be able to this add another NPM script to the package.json.

	...
	"scripts": {
	  ...
	  "cypress:run": "cypress run"
	},
	...
	
Now lets run the command. This will execute all available Cypress tests without opening a browser window and reports back to you command prompt.
	
	npm run cypress:run	

